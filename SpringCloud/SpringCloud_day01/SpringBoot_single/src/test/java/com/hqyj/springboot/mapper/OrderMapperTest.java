package com.hqyj.springboot.mapper;

import com.hqyj.springboot.pojo.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class OrderMapperTest {

    @Autowired
    private OrderMapper orderMapper;


    @Test
    void testInsertOrder() {
        int count = orderMapper.insertOrder(Order.builder().order_name("苹果").order_price(12.16).build());
        System.out.println(count);
    }
}