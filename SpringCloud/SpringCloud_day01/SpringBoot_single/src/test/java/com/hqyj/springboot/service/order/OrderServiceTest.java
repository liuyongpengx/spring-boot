package com.hqyj.springboot.service.order;

import com.hqyj.springboot.pojo.Order;
import com.hqyj.springboot.pojo.Pay;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    @Test
    void testShopping() {
        boolean flag = orderService.shopping(Order.builder().order_name("苹果").order_price(12.16).build(), Pay.builder().payment_name("支付宝").payment_price(12.16).build());
        System.out.println(flag);
    }
}