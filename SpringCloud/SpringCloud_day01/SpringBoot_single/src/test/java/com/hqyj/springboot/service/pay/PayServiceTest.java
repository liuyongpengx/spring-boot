package com.hqyj.springboot.service.pay;

import com.hqyj.springboot.pojo.Pay;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PayServiceTest {

    @Autowired
    private PayService payService;

    @Test
    void payMoney() {
        boolean flag = payService.payMoney(Pay.builder().payment_name("微信").payment_price(12.06).build());
        System.out.println(flag);
    }
}