package com.hqyj.springboot.mapper;

import com.hqyj.springboot.pojo.Pay;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PayMapperTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private PayMapper payMapper;

    @Test
    void contextLoads() {
    }

    @Test
    public void testDataSource(){
        System.out.println(dataSource.getClass().getName());
    }

    @Test
    public void testInsertPayment(){
        int count = payMapper.insertPayment(Pay.builder().payment_name("微信").payment_price(12.06).build());
        System.out.println(count);
    }

}