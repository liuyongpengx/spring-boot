package com.hqyj.springboot.service.pay;

import com.hqyj.springboot.mapper.PayMapper;
import com.hqyj.springboot.pojo.Pay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("payService")
public class PayServiceImpl implements PayService{

    @Autowired
    private PayMapper payMapper;

    @Override
    public boolean payMoney(Pay pay) {
        int count = payMapper.insertPayment(pay);
        if (count != 0){
            return true;
        }
        return false;
    }
}
