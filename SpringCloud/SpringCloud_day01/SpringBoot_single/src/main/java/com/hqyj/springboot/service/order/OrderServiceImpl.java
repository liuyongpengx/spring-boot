package com.hqyj.springboot.service.order;

import com.hqyj.springboot.mapper.OrderMapper;
import com.hqyj.springboot.pojo.Order;
import com.hqyj.springboot.pojo.Pay;
import com.hqyj.springboot.service.pay.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("orderService")
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private PayService payService;

    @Override
    public boolean shopping(Order order, Pay pay) {
        int count = orderMapper.insertOrder(order);
        if (count != 0){
            boolean flag = payService.payMoney(pay);
            if (flag){
                return true;
            }
        }
        return false;
    }
}
