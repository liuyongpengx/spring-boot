package com.hqyj.springboot.service.pay;


import com.hqyj.springboot.pojo.Pay;

public interface PayService {

    boolean payMoney(Pay pay);

}
