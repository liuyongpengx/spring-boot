package com.hqyj.springboot.service.order;

import com.hqyj.springboot.pojo.Order;
import com.hqyj.springboot.pojo.Pay;

public interface OrderService {

    boolean shopping(Order order, Pay pay);

}
