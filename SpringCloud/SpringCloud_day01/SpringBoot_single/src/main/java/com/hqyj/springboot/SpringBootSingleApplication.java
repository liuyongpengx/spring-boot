package com.hqyj.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSingleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSingleApplication.class, args);
    }

}
