package com.hqyj.springboot.controller;

import com.hqyj.springboot.pojo.Order;
import com.hqyj.springboot.pojo.Pay;
import com.hqyj.springboot.pojo.RestfulEntity;
import com.hqyj.springboot.service.order.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping(value = "/shopping")
    public @ResponseBody RestfulEntity<Order> shopping(@RequestBody Order order){
        Pay payment = Pay.builder().payment_price(order.getOrder_price()).payment_name("微信").build();
        boolean flag = orderService.shopping(order, payment);
        RestfulEntity<Order> restfulEntity = new RestfulEntity<Order>();

        if (flag){
            restfulEntity.setCode(200);
            restfulEntity.setMessage("购买商品成功！");
            restfulEntity.setData(order);
        }else {
            restfulEntity.setCode(700);
            restfulEntity.setMessage("购买商品失败！");
        }
        return restfulEntity;
    }
}
