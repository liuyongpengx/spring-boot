package com.hqyj.springboot.service.order;


import com.hqyj.SpringBoot.pojo.Order;
import com.hqyj.SpringBoot.pojo.Pay;

public interface OrderService {

    boolean shopping(Order order, Pay pay);

}
