package com.hqyj.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMultipleOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMultipleOrderApplication.class, args);
    }

}
