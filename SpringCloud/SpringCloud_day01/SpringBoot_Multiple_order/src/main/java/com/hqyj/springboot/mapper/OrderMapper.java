package com.hqyj.springboot.mapper;


import com.hqyj.SpringBoot.pojo.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper {

    int insertOrder(Order order);

}
