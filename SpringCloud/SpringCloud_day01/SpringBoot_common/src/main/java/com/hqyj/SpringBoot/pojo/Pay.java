package com.hqyj.SpringBoot.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Pay {

    private int payment_id;
    private String payment_name;
    private double payment_price;

}
