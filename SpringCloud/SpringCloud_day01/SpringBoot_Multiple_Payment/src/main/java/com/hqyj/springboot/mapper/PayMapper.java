package com.hqyj.springboot.mapper;

import com.hqyj.SpringBoot.pojo.Pay;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PayMapper {

    int insertPayment(Pay payment);
}
