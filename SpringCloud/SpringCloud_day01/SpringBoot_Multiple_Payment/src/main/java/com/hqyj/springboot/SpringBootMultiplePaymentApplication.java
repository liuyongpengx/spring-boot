package com.hqyj.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMultiplePaymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMultiplePaymentApplication.class, args);
    }

}
