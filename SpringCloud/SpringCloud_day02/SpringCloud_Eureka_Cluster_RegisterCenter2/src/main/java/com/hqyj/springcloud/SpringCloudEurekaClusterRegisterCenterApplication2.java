package com.hqyj.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpringCloudEurekaClusterRegisterCenterApplication2 {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEurekaClusterRegisterCenterApplication2.class, args);
    }

}
