package com.hqyj.SpringCloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RestfulEntity<T> {
    private int code;
    private String message;
    private T  data;
}