package com.hqyj.springcloud.service.order;


import com.hqyj.SpringCloud.pojo.Order;
import com.hqyj.SpringCloud.pojo.Pay;
import com.hqyj.SpringCloud.pojo.RestfulEntity;
import com.hqyj.springcloud.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private RestTemplate restTemplate;

    private String url = "http://SPRINGCLOUD-EUREKA-CLUSTER-PAYMENT";


    @Override
    public boolean shopping(Order order, Pay pay) {
        int count = orderMapper.insertOrder(order);
        if (count != 0) {
            ResponseEntity<RestfulEntity> responseEntity = restTemplate.postForEntity(url+"/payMoney", pay, RestfulEntity.class);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                return true;
            }

        }
        return false;
    }
}
