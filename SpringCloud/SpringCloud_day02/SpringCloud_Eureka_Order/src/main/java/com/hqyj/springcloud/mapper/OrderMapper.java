package com.hqyj.springcloud.mapper;



import com.hqyj.SpringCloud.pojo.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper {

    int insertOrder(Order order);

}
