package com.hqyj.springcloud.service.order;


import com.hqyj.SpringCloud.pojo.Order;
import com.hqyj.SpringCloud.pojo.Pay;

public interface OrderService {

    boolean shopping(Order order, Pay pay);

}
