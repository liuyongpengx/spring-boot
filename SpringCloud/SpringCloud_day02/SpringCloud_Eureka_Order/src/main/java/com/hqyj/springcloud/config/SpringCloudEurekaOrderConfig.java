package com.hqyj.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class SpringCloudEurekaOrderConfig {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplateBack(){
        return new RestTemplate();
    }
}
