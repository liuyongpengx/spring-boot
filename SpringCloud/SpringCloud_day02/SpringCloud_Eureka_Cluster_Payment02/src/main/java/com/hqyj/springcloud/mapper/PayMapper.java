package com.hqyj.springcloud.mapper;


import com.hqyj.SpringCloud.pojo.Pay;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PayMapper {

    int insertPayment(Pay payment);
}
