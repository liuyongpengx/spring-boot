package com.hqyj.springcloud.service.pay;


import com.hqyj.SpringCloud.pojo.Pay;

public interface PayService {

    boolean payMoney(Pay pay);

}
