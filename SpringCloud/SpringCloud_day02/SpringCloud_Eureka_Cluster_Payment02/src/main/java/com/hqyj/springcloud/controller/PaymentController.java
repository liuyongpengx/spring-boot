package com.hqyj.springcloud.controller;


import com.hqyj.SpringCloud.pojo.Pay;
import com.hqyj.SpringCloud.pojo.RestfulEntity;
import com.hqyj.springcloud.service.pay.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PaymentController {

    @Autowired()
    private PayService payService;

    @PostMapping(value = "/payMoney")
    public @ResponseBody
    RestfulEntity<Pay> payMoney(@RequestBody Pay pay){
        RestfulEntity<Pay> payRestfulEntity = new RestfulEntity<>();
        boolean flag = payService.payMoney(pay);
        if (flag){
            payRestfulEntity.setCode(200);
            payRestfulEntity.setMessage("购买成功");
        }
        return payRestfulEntity;
    }

}
