package com.hqyj.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringCloudEurekaPaymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEurekaPaymentApplication.class, args);
    }

}
