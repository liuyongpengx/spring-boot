package com.hqyj.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot0401Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot0401Application.class, args);
    }

}
