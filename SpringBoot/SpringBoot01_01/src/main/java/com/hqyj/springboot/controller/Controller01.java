package com.hqyj.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Controller01 {

    @GetMapping("/method01")
    public @ResponseBody String method01(@RequestBody String str) {
        System.out.println(str);

        return "{" + "name:" + "kitty" + "}";
    }

}
