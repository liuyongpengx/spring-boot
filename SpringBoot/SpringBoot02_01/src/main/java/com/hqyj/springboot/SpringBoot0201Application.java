package com.hqyj.springboot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class SpringBoot0201Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot0201Application.class, args);
    }

}
