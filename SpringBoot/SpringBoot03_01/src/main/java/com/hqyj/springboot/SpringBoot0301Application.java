package com.hqyj.springboot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class SpringBoot0301Application {

    public static void main(String[] args) {
        log.info("SpringBoot0301Application ===============> 开始执行");
        SpringApplication.run(SpringBoot0301Application.class, args);
        log.info("SpringBoot0301Application ===============> 结束执行");
    }

}
